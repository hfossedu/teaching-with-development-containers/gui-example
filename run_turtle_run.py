import turtle

pen = turtle.Turtle()
side_length = 150
for square_id in range(10):
    for side_id in range(4):
        pen.forward(side_length)
        pen.right(90)
    side_length -= 10

turtle.exitonclick()
