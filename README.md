# GUI Example

## Use

1. Open this project in VS Code and reopen inside a devcontainer.
2. Open a web browser to http://localhost:6080 and connect with password `vscode`.
3. Inside a terminal in the devcontainer run
    ```
    python run_turtle_run.py
    ```
4. Go back to the browser to view and interact with the GUI.

## How this project was built

Here's how to run GUIs in a devcontainer and view and interact with them
through a browser on the host machine.

1. In your devcontainer.json...
    * Uncomment the "forwardPorts" and add 6080 to its list.

        ```json
        "forwardPorts": [6080],
        ```

    * Add "desktop-lite" as shown below to the "features" section.

        ```json
        "features": {
            "desktop-lite": {
                "password": "vscode",
                "webPort": "6080",
                "vncPort": "5901"
            }
        }
        ```

2. Rebuild your devcontainer.

3. In a browser on your host, open http://localhost:6080 and enter vscode for the password.

4. Run a GUI application inside the container, it will appear in that browser, and it is interactive!

Enjoy!

### Resources

https://technology.amis.nl/software-development/run-and-access-gui-inside-vs-code-devcontainers/
https://github.com/microsoft/vscode-dev-containers/blob/main/script-library/docs/desktop-lite.md
